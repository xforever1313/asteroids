 int MIDDLE = -1;
 int LEFT_EDGE = 1;
 int RIGHT_EDGE = 2;
 int UPPER_EDGE = 3;
 int LOWER_EDGE = 4;

int isAtEdge(float x, float y, float radius){
  int ret = MIDDLE; 
  if(x < radius){
     ret = LEFT_EDGE;
  }
  else if(x >= (width - radius)){
     ret = RIGHT_EDGE; 
  }
  else if (y < radius){
     ret = UPPER_EDGE;
  } 
  else if(y >= (height - radius)){
     ret = LOWER_EDGE;
  } 
  return ret;
}

int isInAsteroid(PVector location, float radius, ArrayList<Asteroid> asteroids){
  int ret = -1;
    for (int i = 0; (i < asteroids.size()) && (ret == -1); ++i){
        if(location.dist(asteroids.get(i).m_location) < (radius + asteroids.get(i).m_radius)){
           ret = i; 
        } 
    }
    return ret;
}
