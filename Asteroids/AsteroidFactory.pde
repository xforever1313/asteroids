class AsteroidFactory{
    ArrayList<PShape> m_shapes;
    ArrayList<PVector> m_shapeCoordinates;
    
    ArrayList<Asteroid> m_asteroids;
    
    final int m_minSize = 20;
    final int m_maxSize = 75;
  
    Boolean isOverLapping(float x, float y, float radius, Car car){
        PVector location = new PVector(x, y);
        Boolean ret = false;
        if (location.dist(car.m_position) < (radius + car.m_boundingCircleRadius)){
            ret = true; 
        }
        else{
             for (int i = 0; i < m_asteroids.size() && !ret; ++i){
                  if(location.dist(m_asteroids.get(i).m_location) < (radius + m_asteroids.get(i).m_radius)){
                      ret = true; 
                  }
             }
        }
        return ret;
    }
    
    AsteroidFactory(){
        m_shapes = new ArrayList<PShape>();
        
        shapeMode(CENTER);
        
        PShape s = createShape();
        
        //25 x 25
        s.beginShape();
        s.vertex(12.5, 0);
        s.vertex(5, 5);
        s.vertex(10,10);
        s.vertex(5, 15);
        s.vertex(5, 20);
        s.vertex(12.5, 25);
        s.vertex(15, 15);
        s.vertex(25, 12.5);
        s.vertex(20, 5);
        s.vertex(15, 5);
        s.endShape(CLOSE);
        s.width = 25;
        s.height = 25;
        m_shapes.add(s);
  
        //75 X 57 asteroid
        s = createShape();
        s.beginShape();
        s.vertex(5, 20);
        s.vertex(10, 35);
        s.vertex(5, 50);
        s.vertex(20, 65);
        s.vertex(30, 55);
        s.vertex(35, 70);
        s.vertex(50, 70);
        s.vertex(55, 55);
        s.vertex(65, 60);
        s.vertex(65, 40);
        s.vertex(60, 30);
        s.vertex(65, 15);
        s.vertex(55, 10);
        s.vertex(45, 15);
        s.vertex(35, 5);
        s.vertex(25, 10);
        s.vertex(20, 5);
        s.vertex(20, 15);
        s.endShape(CLOSE);
        s.width = 75;
        s.height = 75;
        m_shapes.add(s);
  
        //50x50 asteroid
        s = createShape();
        s.beginShape();
        s.vertex(10, 5);
        s.vertex(5, 10);
        s.vertex(5, 20);
        s.vertex(10, 20);
        s.vertex(15, 25);
        s.vertex(5, 30);
        s.vertex(5, 35);
        s.vertex(5, 40);
        s.vertex(10, 45);
        s.vertex(15, 45);
        s.vertex(20, 40);
        s.vertex(25, 50);
        s.vertex(35, 40);
        s.vertex(45, 40);
        s.vertex(45, 30);
        s.vertex(50, 25);
        s.vertex(45, 20);
        s.vertex(45, 10);
        s.vertex(40, 5);
        s.vertex(35, 5);
        s.vertex(25, 0);
        s.vertex(20, 5);
        s.endShape(CLOSE);
        s.width = 50;
        s.height = 50;
        m_shapes.add(s);

        //100x100 asteroid
        s = createShape();
        s.beginShape();
        s.vertex(20,10);
        s.vertex(0,20);
        s.vertex(10,30);
        s.vertex(20,40);
        s.vertex(20,50);
        s.vertex(10,50);
        s.vertex(0,50);
        s.vertex(20,70);
        s.vertex(10,80);
        s.vertex(20,80);
        s.vertex(30,90);
        s.vertex(40,80);
        s.vertex(50,80);
        s.vertex(60,90);
        s.vertex(70,80);
        s.vertex(80,70);
        s.vertex(90,70);
        s.vertex(90, 60);
        s.vertex(100, 50);
        s.vertex(80, 30);
        s.vertex(90, 20);
        s.vertex(80, 10);
        s.vertex(60, 10);
        s.vertex(50, 0);
        s.endShape(CLOSE);
        s.width = 100;
        s.height = 100;
        m_shapes.add(s);
        
        //120 X 120 asteroid
        s = createShape();
        s.beginShape();
        s.vertex(60, 0);
        s.vertex(30, 10);
        s.vertex(20, 20);
        s.vertex(20, 40);
        s.vertex(30, 50);
        s.vertex(10, 60);
        s.vertex(10, 80);
        s.vertex(20, 100);
        s.vertex(30, 110);
        s.vertex(40, 90);
        s.vertex(40, 110);
        s.vertex(60, 100);
        s.vertex(80, 110);
        s.vertex(100, 90);
        s.vertex(80, 90);
        s.vertex(100, 70);
        s.vertex(110, 60);
        s.vertex(100, 50);
        s.vertex(100, 30);
        s.vertex(80, 10);
        s.vertex(60, 20);
        s.endShape(CLOSE);
        s.width = 120;
        s.height = 120;
        m_shapes.add(s);
    }
    
    void updateAsteroids(int numberOfAsteroids, Car car){
       m_asteroids = new ArrayList<Asteroid>();
       for (int i = 0; i < numberOfAsteroids; ++i){
          float x = random(0, width);
          float y = random(0, height);
          
          PShape chosenShape = m_shapes.get(round(random(0, m_shapes.size()-1)));
          
          if (isOverLapping(x, y, chosenShape.width/2, car) || (isAtEdge(x, y, chosenShape.width/2) != MIDDLE)){
              --i;  //Do not add asteroid if its out of bounds or overlapping
          }
          else{
              m_asteroids.add(new Asteroid(x, y, chosenShape));
          }
       }
    }
}
