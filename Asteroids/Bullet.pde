class Bullet{
    PVector m_velocity;
    PVector m_directionVector;
    
    float m_bulletSpeed = 10;
    int m_radius = 3;
    int m_diameter = m_radius * 2; 

    Boolean m_isAlive;
     
    Bullet(PVector position, PVector directionVector, PVector carVelocity){
        m_directionVector = directionVector.get();
        m_velocity = PVector.mult(directionVector, m_bulletSpeed);
        m_velocity.add(carVelocity);
        
        m_directionVector.add(position);
        m_isAlive = true;
    }
    
    void update(ArrayList<Asteroid> asteroids){
        if (m_isAlive){
            PVector nextVector = PVector.add(m_directionVector, m_velocity);
            int asteroidNumber = isInAsteroid(m_directionVector, m_radius, asteroids);
            if((asteroidNumber != -1) && asteroids.get(asteroidNumber).m_isAlive){
                asteroids.get(asteroidNumber).explode();
                m_isAlive = false;
            }
            m_directionVector = nextVector;
        }
    }
    
    void display(){
        if (m_isAlive){
            fill(255, 0, 0);
            noStroke();
            ellipseMode(RADIUS);
            ellipse(m_directionVector.x, m_directionVector.y, m_radius, m_radius);
        } 
    }
    
    Boolean isInCanvas(){
       return (m_directionVector.x >= (0 - m_diameter)) &&
              (m_directionVector.x <= (width + m_diameter)) &&
              (m_directionVector.y >= (0 - m_diameter)) &&
              (m_directionVector.y <= (height + m_diameter));
    }
}
