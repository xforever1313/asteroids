color asteroidFill = color(1, 0, 0);
color asteroidStroke = color(255, 255, 255);

class Asteroid{
  
   PShape m_shape; 
   PVector m_location;
   PVector m_velocity;
   float m_radius;
   Boolean m_isAlive;
   float m_maxSpeed = 3;
   
   Explosion m_explosion;
  
   Asteroid(float x, float y, PShape shape){
      m_shape = shape;
      m_shape.setFill(asteroidFill);
      m_shape.setStroke(asteroidStroke);
      m_radius = shape.width/2;
      m_location = new PVector(x, y);
      m_isAlive = true;
      m_velocity = new PVector(random(-m_maxSpeed, m_maxSpeed), random(-m_maxSpeed, m_maxSpeed));
      m_explosion = new Explosion(m_radius);
   }
   
   void update(){
       if (m_isAlive){
           int changeDirection = isAtEdge(m_location.x, m_location.y, m_radius);
           if ((changeDirection == LEFT_EDGE) || (changeDirection == RIGHT_EDGE)){
               m_velocity.x = -m_velocity.x;
           }
           else if ((changeDirection == UPPER_EDGE) || (changeDirection == LOWER_EDGE)){
               m_velocity.y = -m_velocity.y;
           }
           m_location.add(m_velocity);
       }
       else{
           m_explosion.update(); 
       }
   }
   
   void display(){
     if (m_isAlive){
       pushMatrix();
       translate(m_location.x, m_location.y);
       shapeMode(CENTER);
       shape(m_shape);
       popMatrix();
     }
     else{
         m_explosion.drawExplosion(m_location.x, m_location.y);
     }
   }
   
   void explode(){
        m_isAlive = false;
        score += 10;
   }
}
