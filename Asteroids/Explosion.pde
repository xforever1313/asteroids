class Explosion{

    float m_outerRadius, m_middleRadius, m_innerRadius;
    
    int m_alpha;
    
    int m_count;
  
    Explosion(float radius){
        m_outerRadius = radius;
        m_middleRadius = radius/2;
        m_innerRadius = radius/3.25;
        m_alpha = 254;
        m_count = 0;
    }
    
    void update(){
       if (m_alpha >= 0){
           m_alpha -= 5;
       } 
    }
    
    void drawExplosion(float x, float y){
      noStroke();
      ellipseMode(RADIUS);
      
      if (m_count > 7){
          fill(255, 0, 0, m_alpha);
          ellipse(x, y, m_outerRadius, m_outerRadius);
      }
      
      if (m_count > 3){
          fill(255, 125, 0, m_alpha);
          ellipse(x, y, m_middleRadius, m_middleRadius);
      }
      fill(255, 255, 0, m_alpha);
      ellipse(x, y, m_innerRadius, m_innerRadius);
      
      ++m_count;
    } 
}
