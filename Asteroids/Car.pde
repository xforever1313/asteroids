class Car{
  PVector m_position;
  PVector m_forward;
  PVector m_velocity;
  PVector m_acceleration;
  PVector m_friction;
  
  float m_frictionFactor = 0.01;
  boolean m_accelerating = false;
  boolean m_turningRight = false;
  boolean m_turningLeft = false;
  float m_turnAmt = 0.05;
  float m_size;

  ArrayList<Bullet> m_bullets;
  int m_maxBullets = 3;
  
  float m_boundingCircleRadius;
  
  Boolean m_isAlive;
  
  Explosion m_explosion;

  Car(float x_, float y_, float size_) {
      m_bullets = new ArrayList<Bullet>();
      m_position = new PVector(x_, y_);
      m_velocity = new PVector(0, 0);
      m_forward = PVector.fromAngle(0);
      m_size = size_;
      m_boundingCircleRadius = m_size * 2;
      
      m_isAlive = true;
      m_explosion = new Explosion(m_boundingCircleRadius * 2);
      m_acceleration = new PVector(0,0);
      m_friction = new PVector(0, 0);
  } 

  void update(ArrayList<Asteroid> asteroids) {
    if(isAtEdge(m_position.x, m_position.y, m_boundingCircleRadius) != MIDDLE){
        m_isAlive = false;
    }
    
    int asteroidNumber = isInAsteroid(m_position, m_boundingCircleRadius, asteroids);
    
    if (asteroidNumber != -1){
       if (asteroids.get(asteroidNumber).m_isAlive){
           m_isAlive = false;
        }
    }
    if (m_isAlive){
        if (m_accelerating){
            m_acceleration = PVector.mult(m_forward, 0.25);
        }
        else{
            m_acceleration.mult(0);
        }
        
        if (m_turningRight){
            //rotate the forward vector by turnAmt
            m_forward.rotate(m_turnAmt);
        }
        
        if (m_turningLeft){
             //rotate the forward vector by turnAmt
             m_forward.rotate(-m_turnAmt);
        }
        
        m_velocity.add(m_acceleration);
        m_friction.x = -m_velocity.x * m_frictionFactor;
        m_friction.y = -m_velocity.y * m_frictionFactor;
        m_velocity.add(m_friction);

        // add velocity    
        m_position.add(m_velocity);
           
       for (int i = 0; i < m_bullets.size(); ++i){
            if (m_bullets.get(i).isInCanvas() && m_bullets.get(i).m_isAlive){
                m_bullets.get(i).update(asteroids);
            }
            else{
                m_bullets.remove(i);
            } 
        } 
     }
     else{
         m_explosion.update(); 
     }
  }
  
  void fireBullet(){
      if (m_bullets.size() < m_maxBullets){
          m_bullets.add(new Bullet(m_position, m_forward, m_velocity));
      }
  }

  void display() {
    if (m_isAlive){
      pushMatrix();
      translate(m_position.x, m_position.y);  
      rotate(m_forward.heading( ));
      fill(0);
      stroke(255);
      beginShape();    
      vertex(m_size*2, 0); //Nose
      vertex(-m_size*1.5, m_size); //Right fin
      vertex(-m_size, 0);  //Tail
      vertex(-m_size*1.5, -m_size); //left fin 
      endShape(CLOSE);
      popMatrix();
      
      for (int i = 0; i < m_bullets.size(); ++i){
          m_bullets.get(i).display(); 
      }
    }
    else{
        m_explosion.drawExplosion(m_position.x, m_position.y);  
    }
  }
}

