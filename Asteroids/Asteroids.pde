int score;

Boolean settingUp;

Car car;

AsteroidFactory asteroids;

Boolean begin;

void setup() {
  size(800, 600, P2D);
  asteroids = new AsteroidFactory();
  begin = false;
  background(0);
  textSize(72);
  textAlign(CENTER, CENTER);
  fill(255, 255, 0);
  text("ASTEROIDS\n\nPress \'n\' to start", width/2, height/2);
}

void reset(){
    settingUp = true;
    score = 0;
    background(0);
    textSize(72);
    textAlign(CENTER, CENTER);
    fill(255, 255, 0);
    text("LOADING...", width/2, height/2);  
    car = new Car(width/2, height/2, 8);
    asteroids.updateAsteroids(5, car);
    settingUp = false;
    begin = true;
}

//Integer i = 0;

void drawScore(){
    fill(255);
    textSize(50);
    textAlign(LEFT, LEFT);
    text(score, 20, 100); 
}

void drawGameOver(){
    textSize(72);
    textAlign(CENTER, CENTER);
    fill(255, 255, 0);
    text("GAME\nOVER\n\nPress \'n\' to try again", width/2, height/2);
}

void updateAsteroids(){
    for (int i = 0; i < asteroids.m_asteroids.size(); ++i){
        asteroids.m_asteroids.get(i).update();
        if (asteroids.m_asteroids.get(i).m_explosion.m_alpha <= 0){
            asteroids.m_asteroids.remove(i); 
        }
    }
    if (asteroids.m_asteroids.size() == 0){
        asteroids.updateAsteroids(5, car); 
    }
}

void drawAsteroids(){
    for (int i = 0; i < asteroids.m_asteroids.size(); ++i){
        asteroids.m_asteroids.get(i).display();
    }  
}

void draw() {
  if (begin){
    if (!settingUp){
      background(0);
      updateAsteroids();
      car.update(asteroids.m_asteroids);
      drawAsteroids();
      car.display();
      drawScore();
      if (!car.m_isAlive){
          drawGameOver(); 
      }
    }
  }
}

void keyReleased() {
  if ((key == CODED) && begin && !settingUp) {
    if (keyCode == UP) {
      car.m_accelerating = false;
    }
    else if (keyCode == RIGHT) {
      car.m_turningRight = false;
    }
    else if (keyCode == LEFT) {
      car.m_turningLeft = false;
    }
  }
} 

void keyPressed() {
  if ((key == CODED) && begin && !settingUp) {
    if (keyCode == UP) {
      car.m_accelerating = true;
    }
    else if (keyCode == RIGHT) {
      car.m_turningRight = true;
    }
    else if (keyCode == LEFT) {
      car.m_turningLeft = true;
    }
  }
}

void keyTyped(){
    if (key == ' '){
        if (begin && !settingUp){
            car.fireBullet();
        }
    }
    else if (key == 'n'){
        reset(); 
    } 
}

void mouseClicked(){
    if((mouseButton == LEFT) && begin && !settingUp){
        car.fireBullet(); 
    }
}
