abstract class ThreadHelper extends Thread{
   boolean keepRunning;
  
   ThreadHelper(){
       keepRunning = true; 
    }  

    synchronized boolean isDead(){
        boolean dead = !keepRunning;
        return dead;
    }

    synchronized void kill(){
        keepRunning = false; 
    }
}


