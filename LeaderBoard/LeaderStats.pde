class LeaderStats implements Comparable<LeaderStats> {
    Integer score;
    String name;
    
    LeaderStats (String name, Integer score) {
        this.name = name;
        this.score = score;
    } 
    
    Integer getScore() {
        return score; 
    }
    
    String getLeaderName() {
        return name;
    }
    
    int compareTo(LeaderStats other) {
        int ret = 0;
        if (this.score < other.score) {
            ret = 1;
        }
        else if (this.score > other.score) {
            ret = -1; 
        }
        return ret;
    }
}
