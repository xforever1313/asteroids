class ListenSocket extends ThreadHelper {
    ServerSocket listenSocket;
    DataSocket dataSocket;
    
    int port;
    
    ListenSocket(int port) {
       listenSocket = null;
       this.port = port;
       dataSocket = null; 
    }
    
    void run() {
        try {
            listenSocket = new ServerSocket(port);

            while (!isDead()) {
                Socket s = listenSocket.accept();
                if (dataSocket != null) {
                    dataSocket.kill();
                    dataSocket.join(); 
                }
                dataSocket = new DataSocket(s);
                dataSocket.start();
            }
        }
        catch (SocketException e){
            println(e.getCause()); 
        }
        
        catch (IOException e) {
            println(e.getCause()); 
        }
        catch (InterruptedException e) {
            println(e.getCause()); 
        }
        
        //Leaving loop
        if (dataSocket != null) {
             dataSocket.kill();
             try {
                 dataSocket.join(); 
             }
             catch (InterruptedException e) {
             }
        }
    }
    
    synchronized void kill(){
      try {
        listenSocket.close();
      }

      catch(IOException e){
      }
      super.kill();
    }
}
