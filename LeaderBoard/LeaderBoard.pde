import java.net.*;
import java.util.*;
import java.io.InputStreamReader;
import java.util.Collections;

ArrayList<LeaderStats> stats;

final int totalLeaders = 10;

ListenSocket listenSocket;

void setup() {
    size(400, 750);
    frameRate = 10;
    stats = new ArrayList<LeaderStats>();
    listenSocket = new ListenSocket(9009);
    listenSocket.start();
}

void addStat(LeaderStats stat) {
    stats.add(stat);
    Collections.sort(stats);
    if (stats.size() > totalLeaders) {
        stats.remove(stats.size() - 1);
    }
}

void draw() {
    background(0);
    textSize(25);
    textAlign(LEFT, TOP);
    fill(255);
    text("Asteroid Leaderboard!", 50, 25);
    for (Integer i = 1; (i - 1) < stats.size(); ++i) {
         text("#" + i.toString() + ": " + stats.get(i - 1).getScore().toString() + " - " + stats.get(i - 1).getLeaderName(), 50, 50 * (i + 1));
    }
}

void exit() {
    listenSocket.kill();
    try {
      listenSocket.join();
    }
    catch (InterruptedException e){
    }
    super.exit(); 
}
