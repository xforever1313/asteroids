class DataSocket extends ThreadHelper {
   Character startChar = '\u0002';
   Character endChar = '\u0003';
    
   Socket dataSocket;
   DataSocket(Socket s) {
       this.dataSocket = s;
   } 
   
   void run() {
     try {
         BufferedReader reader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
         StringBuilder sb = null;
         while (!isDead()) {
             if (reader.ready()) {
                 int ch = reader.read();
                 while (ch != -1) {
                     if (ch == startChar) {
                         sb = new StringBuilder(); 
                     }
                     else if ((ch == endChar) && (sb != null)) {
                         //Make new person
                         String [] stats = sb.toString().split("\t");
                         addStat(new LeaderStats(stats[0], Integer.parseInt(stats[1])));
                     }
                     else if (sb != null) {
                         sb.append((char)ch); 
                     }
                     ch = reader.read();
                 } 
             }
         }
     }
     catch (IOException e) {
       
     }
   }
   
   synchronized void kill() {
       try {
           dataSocket.close();
       } 
       catch (IOException e) { 
       }
       super.kill();
   }
}
