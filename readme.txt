Asteroids
Seth Hendrick

Instructions:
    Press 'n' at the start screen to start a new game
    Press the up arrow to accelerate
    Press left or right to rotate the shit
    Press the space bar to fire a bullet.  
        Only 3 bullets are allowed on the screen at the same time.  You have been warned.
    Unlike the original asteroids, hitting a wall will blow up your ship.  Asteroids also bounce off the walls.  Asteroids also do not break up into smaller pieces
    Like the original asteroids, if an asteroid hits your ship, you blow up.

Design:
    There are 5 classes.  Car, Asteroids, AsteroidFactory, Bullet, and Explosion.
    
    Car is the ship.  It is the modified version of the car given in class.  The car contains 5 vectors: position, forward, velocity, acceleration, and friction.  The car has two states: alive and not alive.  It also contains two methods: update() and display().  When the car is not alive, the update method will call update on the car's explosion object (see below), and the display method will show the explosion.  When the car is alive, the update method calculates the car's next position based on the current position, velocity, acceleration, and friction.  It will also turn and/or accelerate if the user demands it.  The display() method, when the car is alive, will draw the car and any bullets. 
    The car class also contains a fireBullet() method.  This will create a bullet if there are not already too many bullets on the screen.  This is to increase difficulty, as the user cannot spam bullets, and must use them wisely.  When the bullet leaves the screen, it is destroyed.
    
    The asteroid factory generates the 5 pre-configured asteroids when it is constructed.  When the method "updateAsteroid" is called, it constructs asteroids using a randomly chosen pre-configured asteroid shape for each asteroid.  It also determines a random x and y to put the asteroid.  If the generated x and y are occupied by the ship or another asteroid, it tries again.
    
    An asteroid takes a starting x, y, and a preconfigured asteroid pshape in its constructor.  It then generates a random velocity, which never changes.  The asteroid has an update and a display method, similar to the car. It also has two states, like the car (alive and not alive), and a velocity and position vector.  When update() is called, the asteroid's position gets modified based on the velocity if it�s alive.  If the asteroid is at an edge, the velocity is altered as needed.  Meanwhile, if the asteroid is destroyed, an update is called on the resulting explosion (see below).  The asteroid's display method will display the pshape when the asteroid is alive, or the explosion when the asteroid is destroyed.
    
    A bullet is similar to the asteroid in the fact that it has a velocity and position vector (called m_directionVector in the code).  It also has an alive and not alive state, and a display() and update() method.  When constructed, it copy's the car's forward vector, as that�s the direction the bullet needs to travel.  Its update() method peeks at the next position the bullet is going to land.  If it ends up in an asteroid's bounding circle, the bullet ends up in the not alive state, and the asteroid explodes.  Otherwise, the position is updated based on the velocity.  The display method simply shows an ellipse at the position.
    
    The car and asteroid both have an explosion object.  The constructor of Explosion takes a radius, which is the size of the explosion.  When update() is called, the alpha of the explosion simply decreases.  Display() will display 1-3 circles based on how old the explosion is.
    
    For the collision logic, the ship and the asteroids have bounding circles surrounding them.  If the ship�s bounding circle crosses an asteroid's the ships, the ship blows up.  If the bounding circle hits an edge, the ship will blow up, and the asteroid will change directions.  If a bullet enters an asteroid's bounding circle, the asteroid will explode.  It was determined that this is enough of a check to blow up an asteroid.  Another idea was to check the pixel of the next bullet to see if it would end up in an asteroid, but checking the bounding circle appears good enough.  
